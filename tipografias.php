<!DOCTYPE html>
<html>
<head>
	<title>Pruebas Bootstrap</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


</head>
<body>

	<h1>Hola Bootstrap</h1>
	<h2>Hola Bootstrap</h2>
	<h3>Hola Bootstrap</h3>
	<h4>Hola Bootstrap</h4>
	<h5>Hola Bootstrap</h5>



	<h1 class="display-1">Hola Bootstrap</h1>
	<h2 class="display-2">Hola Bootstrap</h2>
	<h3 class="display-3">Hola Bootstrap</h3>
	<h4 class="display-4">Hola Bootstrap</h4>
	<h5 class="display-5">Hola Bootstrap</h5>


	<h1 class="display-1">Hola <span class="text-muted">Bootstrap</span></h1>
	<p>sadsadadasdasdasdasdasdsadasdasd</p>
	<p class="font-weight-bold">sadsadadasdasdasdasdasdsadasdasd</p>
	<p class="font-weight-normal">sadsadadasdasdasdasdasdsadasdasd</p>
	<p class="font-italic">sadsadadasdasdasdasdasdsadasdasd</p>

	<p class="text-uppercase">sadsadadasdasdasdasdasdsadasdasd</p>
	<p class="text-lowecase">sadsadadasdasdasdasdasdsadasdasd</p>
	<p class="text-capitalize">sadsadadasdasdasdasdasdsadasdasd</p>



	<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></blockquote>

	<blockquote><p class="blockquote">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></blockquote>




	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>